public class Classwork3 {
    public void massiv(){
        int [] massiv1 = new int [100];
        for (int i = 0; i < massiv1.length; i++) {
            massiv1 [i] = i;
        }

        for (int i = 0; i <= massiv1.length - 1; i++) {
            System.out.print(massiv1[i] + " ");
        }

        System.out.println();

        for (int i = massiv1.length -1; i >= 0; i--) {
            System.out.print(massiv1[i] + " ");
        }
    }

    public void massiv2(){
        System.out.println();
        int [] massiv2 = new int [100];
        for (int i = 0; i < massiv2.length; i++) {
            massiv2 [i] = i;
        }

        for (int i = 0; i <= massiv2.length - 1; i++) {
            if (i % 2 == 0) {
                System.out.print(massiv2[i] + " ");
            }
        }

        System.out.println();

        for (int i = massiv2.length -1; i >= 0; i--) {
            if (i % 2 != 0) {
                System.out.print(massiv2[i] + " ");
            }
        }
    }
}
